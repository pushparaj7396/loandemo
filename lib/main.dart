import 'package:flutter/material.dart';
import 'package:loan_app/view/LoginScreen.dart';
import 'package:provider/provider.dart';
import 'package:loan_app/controller/login_controller.dart';
void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
      ],
    child: MaterialApp(
      title: 'Loan Application',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: MyCustomSplashScreen(),
      home: LoginScreen(),
      
    ),
    );
  }
}


