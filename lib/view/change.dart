
import 'package:flutter/material.dart';
import 'package:loan_app/view/dashboard.dart';
import 'package:loan_app/controller/login_controller.dart';
import 'package:loan_app/common/constants.dart';
import 'package:loan_app/common/common_function.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool showLoader = false;
  bool passwordVisibility = false;
  final _formKey = GlobalKey<FormState>();
  
    TextEditingController phonenumber = TextEditingController();
    TextEditingController password = TextEditingController();
    var loading = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const[
          CircularProgressIndicator(),
          Text(" Authenticating ... Please wait")
        ],
      );
    doLogin () async {
      setState(() {
        showLoader = true;
      });
      final AuthProvider auth = Provider.of<AuthProvider>(context);
      final Future<Map<String, dynamic>> successfulMessage =
          auth.login(phonenumber.text, password.text);
          successfulMessage.then((response) async {
        if (response['status'] == true) {            
          displayToastCustom(
            context, Constants.appColor, "Successfully Logged In...");
          Navigator.pushAndRemoveUntil(
            context,
            PageTransition(
              alignment: Alignment.bottomCenter,
              curve: Curves.easeInOut,
              duration: const Duration(milliseconds: 300),
              reverseDuration: const Duration(milliseconds: 300),
              type: PageTransitionType.rightToLeft,
              child: const Dashboard(),
              childCurrent: LoginScreen(),
              
            ),
            (Route route) => false,
          );
        } else {
          displayToastCustom(
          context, Constants.appColor, "Phonenumber and Password  Incorrect");
        }
      });
    
    }  
    
  @override
    Widget build(BuildContext context) {
      Constants.height = MediaQuery.of(context).size.height;
      Constants.width = MediaQuery.of(context).size.width;
       final AuthProvider auth = Provider.of<AuthProvider>(context);
      
      // ignore: prefer_function_declarations_over_variables
      
      return Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Image.asset(
              'assets/background.jpeg',
              fit: BoxFit.cover,
              color: Colors.black54,
              colorBlendMode: BlendMode.darken,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                    children: <Widget>[
                      SingleChildScrollView(
                        child: Container(
                        height: 500,
                        width: 350,
                        
                        padding: const EdgeInsets.symmetric(
                          horizontal: 30,
                          vertical: 25,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                        ),
                        child: SafeArea(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                              const Text(
                                  'LOGIN',
                                  style: TextStyle(color: Color.fromARGB(255, 34, 34, 35), fontSize: 18,fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Image.asset(
                                  'assets/login.png',
                                  width: 80,
                                  height: 100,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                  'Type Phone Number and Password to continue',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Color.fromARGB(255, 34, 34, 35), fontSize: 18),
                                ),
                                getTextFields(),
                                 getSubmitButton(auth),
                                //getSubmitButton()
                              ],
                            ),
                        ),
                        ),
                      ),
                      ),
                    ],
                  ),
              ],
            ),
          ],
        ),
      );
    }
  
  Widget getTextFields(){
    //var phonenumber;
    return Column
    (
      children: 
      [
        Padding
        (
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
          child: Material
          (
            elevation: 5,
            borderRadius: const BorderRadius.all(
              Radius.circular(10.0), 
            ),
            color: Constants.bgColor,
            child: TextFormField
            (
             controller:phonenumber,
              autocorrect: false,
              autofocus: false,
              style: const TextStyle(
                fontSize: 18,
              ),
              keyboardType: TextInputType.number,
              
              decoration: InputDecoration
              (
                fillColor: Colors.white,
                filled: true,
                // border: OutlineInputBorder(
                //   borderRadius: BorderRadius.circular(30),
                // ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide:
                        const BorderSide(color: Color(0xFFEBEBEB), width: 1)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide:
                        const BorderSide(color: Color(0xFFEBEBEB), width: 1)),
                prefixIcon: const Icon(
                  Icons.person,
                  size: 22,
                  color: Color(0xFF5A5A5B),
                ),
                hintText: "Username",
                
              ),
            ),
            
          ),
         
        ),
        
        Padding
        (
          padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 8),
          child: Material
          (
            elevation: 5,
            borderRadius: const BorderRadius.all(
              Radius.circular(10.0), 
            ),
            color: Constants.bgColor,
            child: TextFormField
            (
              controller: password,
              autocorrect: false,
              autofocus: false,
             obscureText: !passwordVisibility,
              style: const TextStyle(
                fontSize: 18,
              ),
              keyboardType: TextInputType.number,
              decoration: InputDecoration
              (
                fillColor: Colors.white,
                filled: true,

                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide:
                        const BorderSide(color: Color(0xFFEBEBEB), width: 1)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide:
                        const BorderSide(color: Color(0xFFEBEBEB), width: 1)),
                prefixIcon: const Icon(
                  Icons.lock,
                  size: 22,
                  color: Color(0xFF5A5A5B),
                ),
                suffixIcon: IconButton(
                  icon: Icon(
                    passwordVisibility
                        ? Icons.visibility_off
                        : Icons.visibility,
                    size: 22,
                    color: const Color(0xFF5A5A5B),
                  ),
                  onPressed: () {
                    setState(() {
                      passwordVisibility = !passwordVisibility;
                    });
                  },
                ),
                hintText: "Password"
              ),
            )
          )
        )
      ],
    );
  }
  
  Widget getSubmitButton(auth) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            if (_formKey.currentState!.validate() && !showLoader ) {
              doLogin();
            }
            
          },
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: const LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Constants.appColor, Constants.appColor])),
            height: 50,
            width: Constants.width / 2.5,
            child: Center(
              // child: showLoader
              //     ? const CircularProgressIndicator(
              //         color: Colors.white,
              //       )
              //     : const Text(
              //         'Login',
              //         style: TextStyle(
              //             fontWeight: FontWeight.bold,
              //             fontSize: 18,
              //             color: Colors.white),
              //       ),
              // child: Text(
              //   "Login",
              //   style: TextStyle(
              //     fontWeight: FontWeight.bold,
              //     fontSize: 18,
              //     color:Colors.white
              //    ),
              //   ),
               child : auth.loggedInStatus == Status.Authenticating
                ? loading 
                : const Text(
                "Login",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color:Colors.white
                 ),
                ),
            ),
          ),
        ),
        
      ],
    );
  }

}

