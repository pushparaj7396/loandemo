import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Color.fromARGB(255, 237, 235, 235), // <-- This works for fixed
          selectedItemColor: Colors.black,
          type: BottomNavigationBarType.fixed,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home,color:Color.fromARGB(255, 88, 51, 237)),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.menu_book,color:Color.fromARGB(255, 88, 51, 237)),
              label: 'Apply Loan',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person,color:Color.fromARGB(255, 88, 51, 237)),
              label: 'Customers',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.report,color:Color.fromARGB(255, 88, 51, 237)),
              label: 'Report',
            ),
          ]
        ),
      //),
      
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 88, 11, 241),
        elevation: 0.0,
        leading: Builder(
          builder: (context) => IconButton(
            icon:const Icon(Icons.menu),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        title: const Text(
          "Dashboard",
          style:TextStyle(color: Colors.white, fontSize: 15)
        ),
      ),
      
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 88, 11, 241)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget> [
          const SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const[
                  
                 Center(
                    child: Text(
                      "Total Amount",
                      style: TextStyle(color: Colors.white, fontSize: 30,fontWeight: FontWeight.bold),
                    ),
                  ),
                  Center(
                    child: Text(
                      "1,20,000",
                      style: TextStyle(color: Colors.white, fontSize: 30,fontWeight: FontWeight.bold),
                    ),
                  ),
                 SizedBox(height: 50),
                ],
              ),
            ),
            Expanded(
              child: Container(
                decoration:const BoxDecoration(
                  color:Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30),topRight: Radius.circular(30))
                ),
                
              ),
              
            ),
            // Column(
            //   children: <Widget>[
            //     CarouselSlider(
            //       items: [
            //           Container(
            //             margin: const EdgeInsets.all(6.0),
            //             decoration: BoxDecoration(
            //               borderRadius: BorderRadius.circular(8.0),
            //               shape: BoxShape.rectangle,
            //               color: Colors.green,
            //             ),
            //              child: const Text(
            //                 "Total Customers",
            //                 style: TextStyle(color: Colors.white,fontSize:20),
                            
            //               ),
                         
            //           ),
            //       ],
            //       options: CarouselOptions(
            //         height: 100.0,
            //         enlargeCenterPage: true,
            //         autoPlay: true,
            //         aspectRatio: 16 / 9,
            //         autoPlayCurve: Curves.fastOutSlowIn,
            //         enableInfiniteScroll: true,
            //         autoPlayAnimationDuration: Duration(milliseconds: 800),
            //         viewportFraction: 0.8,
            //       ),
            //     )
            //   ],
            // ),
          ],
       ),
      ),
      drawer: Drawer(
        child: _drawerList(),
      ),
      
    );
  }
}
_drawerList() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  'Loan App',
                ),
              ],
            ),
            
          ),
         const ListTile(
            // Some Code
          ),
        ],
      ),
    );
  }

 