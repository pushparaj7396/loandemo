
class User 
{
  int? phonenumber;
  int? password;
 

  User({this.phonenumber, this.password});

  factory User.fromJson(Map<String, dynamic> responseData) {
    return User(
        phonenumber: responseData['phonenumber'],
        password: responseData['password'],
        
    );
  }
}