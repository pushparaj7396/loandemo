
import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:fluttertoast/fluttertoast.dart';

printIfDebug(var printStatement) {
  if (Constants.debug) {
    // ignore: avoid_print
    print(printStatement.toString());
  }
}

displayToastCustom(context, Color toastColor, String text,
    [Color textColor = Colors.white]) {
  FToast fToast = FToast();
  fToast.init(context);
  Widget toast = Container(
    padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10.0),
      color: toastColor,
    ),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          text,
          maxLines: 3,
          style: TextStyle(
            color: textColor,
          ),
        ),
      ],
    ),
  );

  fToast.showToast(
    child: toast,
    gravity: ToastGravity.BOTTOM,
    toastDuration: const Duration(seconds: 2),
  );
}
