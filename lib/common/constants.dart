import 'package:flutter/material.dart';

class Constants {
 
 static bool debug = true;
 static double height = 0.0;
  static double width = 0.0;
 static String domain = "https://loanapp-production.herokuapp.com";
  static const Color appColor = Color.fromARGB(255, 81, 31, 244);
   static const Color bgColor = Color(0xFFF2F2F2);
  static const Color appOrangeColor =
      Color.fromARGB(255, 183, 32, 46); //Color.fromARGB(255, 247, 148, 30);
  static const Color appRedColor = Color.fromARGB(255, 183, 32, 46);
  static const Color appSecondaryColor = Color(0xFF3F6876);
  static int phonenumber = 9999999999; 
  static const String login = 'https://loanapp-production.herokuapp.com/userdetails';
}
