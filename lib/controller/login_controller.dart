import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:loan_app/model/login.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

enum Status {
  NotLoggedIn,
  NotRegistered,
  LoggedIn,
  Authenticating,
  Registering,
  LoggedOut
}
class UserProvider with ChangeNotifier {
  User _user = User();

  User get user => _user;

  void setUser(User user) {
    _user = user;
    notifyListeners();
  }
}



class AuthProvider with ChangeNotifier {

  Status _loggedInStatus = Status.NotLoggedIn;

  Status get loggedInStatus => _loggedInStatus;


  Future<Map<String, dynamic>> login(phonenumber, password) async {
    var result;

  
    
    // _loggedInStatus = Status.Authenticating;
    // notifyListeners();
    
    
    var headers ={'Content-Type': 'application/json'};
    var request =  http.Request('GET', Uri.parse('https://loanapp-production.herokuapp.com/userdetails'));
      request.body = json.encode({
        "phonenumber": phonenumber,
        "password": password,
        "result": 1,
      });
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    
    //response.statusCode
    
    if (response.statusCode == 200) 
    {
      String responseString = await response.stream.bytesToString();
      Map responseJson = jsonDecode(responseString);
      if(responseJson["code"] == 200)
      {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setInt('phonenumber', responseJson['data']['phonenumber']);
          _loggedInStatus = Status.LoggedIn;
          notifyListeners();
          result = {'status': true, 'message': 'Successful'};
      }
      else
      {
          _loggedInStatus = Status.NotLoggedIn;
          notifyListeners();
          result = {
            'status': false,
            'message': 'Username and Password Incorrect'
          };
      }
    } 
    else
    {
      _loggedInStatus = Status.NotLoggedIn;
      notifyListeners();
      result = {
        'status': false,
        'message': 'Username and Password Incorrect'
      };
    }
    
    return result;
    
  }

  static onError(error) {
    print("the error is $error.detail");
    return {'status': false, 'message': 'Unsuccessful Request', 'data': error};
  }

  
}


